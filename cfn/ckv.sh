#!/bin/sh

CMD="checkov --framework cloudformation --external-checks-dir . -c simple -f cfn.yaml --compact"
echo "### Executing: $CMD"

# Checkov scan
$CMD
