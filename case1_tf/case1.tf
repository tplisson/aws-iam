### Scenario 1 
##  IAM Policy documents that have statements granting any s3 actions 
## that have a * in any of the specified resources for that statement.

resource "aws_iam_role_policy" "scenario_1_fail1" {
  name   = "scenario-1-fail1"
  role   = aws_iam_role.example_role.id
  policy = data.aws_iam_policy_document.scenario_1_fail1.json
}

data "aws_iam_policy_document" "scenario_1_fail1" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = ["*"] ### FAIL
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
}

data "aws_iam_policy_document" "scenario_1_fail2" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = ["arn:aws:s3:::cnc-*"] ### FAIL
  }
}

data "aws_iam_policy_document" "scenario_1_fail3" {
  statement {
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
  statement {
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = ["arn:aws:s3:::cnc-*"] ### FAIL
  }
}

#######################################################

resource "aws_iam_role_policy" "scenario_1_pass1" {
  name   = "scenario-1-pass1"
  role   = aws_iam_role.example_role.id
  policy = data.aws_iam_policy_document.scenario_1_pass1.json
}

data "aws_iam_policy_document" "scenario_1_pass1" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
}

data "aws_iam_policy_document" "scenario_1_pass2" {
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-foo-bar",
      "arn:aws:s3:::cnc-example-bucket-foo-bar/*" ### PASS
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*"
    ]
    resources = [
      "arn:aws:s3:::cnc-example-bucket-bar-foo",
      "arn:aws:s3:::cnc-example-bucket-bar-foo/*" ### PASS
    ]
  }
}

data "aws_iam_policy_document" "scenario_1_pass3" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole", ### PASS
    ]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}



# ^([a-zA-Z0-9-:]*?\*[a-zA-Z0-9-:]*?\/?.*)$

# "arn:aws:s3:::cnc-*"
# "arn:aws:s3:::cnc-*/*"
# "arn:aws:s3:::cnc-example*"
# "arn:aws:s3:::cnc-example-bucket-foo-bar"
# "arn:aws:s3:::cnc-example-bucket-foo-bar/*"
# "arn:aws:s3:::cnc-example-bucket-foo-bar/prefix*"
# "arn:aws:s3:::cnc-example-bucket-foo-bar/folder/*"

# https://www.rexegg.com/regex-quickstart.html
# https://regex101.com/r/qV405n/1