#!/bin/sh

# # CLI usage
# if [ $# -lt 1 ]
# then
#   echo 1>&2 "$0: not enough arguments\n"
#   echo "Usage: ckv.sh <check_id>\n"
#   exit 1
# fi

# Variables settings
ID=5

CMD="checkov --framework terraform --external-checks-dir . -c case$ID -f case5_alt.tf --compact"
# CMD="checkov --framework terraform --external-checks-dir . -c case$ID -f case$ID.tf"
# CMD="checkov --framework terraform --external-checks-dir . -c case$ID -f case$ID.tf --compact"
# CMD="checkov --framework terraform_plan --external-checks-dir . -c case$ID -d . --compact"
echo "### Executing: $CMD"

# Checkov scan
$CMD
